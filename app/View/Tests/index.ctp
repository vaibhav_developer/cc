
<div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Tables
            <small>advanced tables</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Hover Data Table</h3>
            <input type="submit" class="btn btn-primary pull-right" value="Post A Job" data-toggle="modal" data-target="#add_company" style="margin: 11px 11px 0px 0px ;">                                   
                                
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                      <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Rendering engine</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Browser</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Platform(s)</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Engine version</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">CSS grade</th></tr>
                    </thead>
                    <?php 
                      foreach($item as $val)
                      {
                        ?>
                        
                           <tbody>
                           <tr role="row" class="odd">
                        <td class="sorting_1"><?php echo $val['Test']['name'];?></td>
                        <td><?php echo $val['Test']['category'];?></td>
                        <td><?php echo $val['Test']['price'];?></td>
                        <td><a href="#" data-target="<?php echo $val['Test']['id'] ;?>"> Edit</a> <a href="#">delete</a></td>
                        <td>A</td>
                      </tr>
                           
                </tbody>
                        <?php
                      }
                    ?>
                 
                    
                  </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

             
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!--  start model add company -->
               
                        <div class="modal fade" id="add_company">
                          <div class="modal-dialog" >
                            <div class="modal-content" >
                             <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <h4 class="modal-title">Post A Job</h4>
                             </div>
                              <div class="modal-body">
                                 <div class="row">
                         <!-- Form start Change basic information-->
                        <div class="col-md-12">
                        <div id="alert"></div>
                            <!-- general form elements -->
                            <div class="box box-primary">
                                
                                <!-- form start -->
                                
                               <?php echo $this->Form->create('Test',array('id'=>'add_company_job','class'=>'form-horizontal'));?>
                                    <div class="box-body">
                                        <div class="form-group">
                                         
                                     <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="control-label" >Product Name</label>
                                             <input type="text" name="name" class="form-control" /> 
                                          </div>
                                           <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="control-label" >Company Name</label>
                                                <input type="text" name="category" class="form-control" /> 
                                          </div>
                                           <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="control-label" >Company Name</label>
                                                <input type="text" name="price" class="form-control" /> 
                                          </div>
                                       
                                           
                                      
                                    <!-- /.box-body -->
                                 
                                   
                               
                            </div><!-- /.box -->
                      </div><!--/.col form end --> 
                              
                              </div>
                              </div>
                              <div class="modal-footer">
                               <div class="box-footer text-right">
                                     <img src="" id="add_com" class="hide" style="padding-right: 5px;"  />
                                        <input type="submit" class="btn btn-primary "  value="Submit">
                                    </div>
                                     </form>
                              </div>
                            <!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /end.modal -->  
                        </div>
                        </div>
                        
                        <script>
                            $(document).ready(function() {
                                
                                  $('#add_company_job').submit(function(event) {
               event.preventDefault();
              
        
               
                formData =  $(this).serialize();
                $('#add_com').removeClass('hide');
                $.ajax({
                type: "POST",
                url: "<?php echo Router::url(array('controller' => 'tests', 'action' =>'add_tests'), true); ?>",
                data: formData,
                dataType: 'json', 
                success: function(res) {
                   
                   if(res.status=="1")
                  {
                    $('#alert').html('<div class="alert alert-success alert-dismissible" id="s-msg" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+res.massage+'</div>')
                    setTimeout(function(){ $('#s-msg').fadeOut() }, 3000);
                   $('#add_company_job')[0].reset();
                    jobs_listing.fnDraw();
                 
                  }
                  else{
                     $('#alert').html('<div class="alert alert-danger alert-dismissible" id="s-msg1" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+res.massage+'</div>')
                   
                  }
                  $('#add_com').addClass('hide');
                  
                     }
                        
                })
                 
             });
                            });
                        </script>