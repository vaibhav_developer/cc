<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>Admin</b>LTE</a>
      </div><!-- /.login-logo -->
     	<div id="login-inner">
	<?php echo  $this->Form->create('User',array('novalidate'=>true,'inputDefaults'=>array('div'=>false,'label'=>false)));?>
		<table border="0" cellpadding="0" cellspacing="0">
		
		<tr>
			<th>Email</th>
			<td><?php echo  $this->Form->input('email',array('class'=>'login-inp'));?></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><?php echo  $this->Form->input('password',array('class'=>'login-inp'));?></td>
		</tr>
		<tr>
			<th></th>
			<td valign="top"><input type="checkbox" class="checkbox-size" id="login-check" /><label for="login-check">Remember me</label></td>
		</tr>
		<tr>
			<th></th>
			<td><?php echo  $this->Form->submit('Submit',array('class'=>'submit-login'));?></td>
		</tr>
		</table>
		<?php echo  $this->Form->end();
	?>
	</div>
    </div><!-- /.login-box -->
