<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class TestsController extends AppController {
	public function beforeFilter()
	{
	parent::beforeFilter();
		$this->Auth->allow(array(''));
	
	//echo AuthComponent::password('123456');die;
	}

	public function index()
    {
        $this->loadModel('Test');
        $res = $this->Test->find('all');
        $this->set('item',$res);
        
    }	
			
	public function add_tests()
    
    {
         if ($this->request->is('ajax')) {
            $this->layout = false;
            $this->autoRender = false;
           // pr($this->request->data);die;
            if ($this->Test->save($this->request->data)) {
               
                      $result['status'] = '1';
				      $result['massage'] = "success";
                    
                   }else
                   {
                      $result['status'] = '0';
				      $result['massage'] = "error";
                    
                   }
                   echo json_encode($result);
          }
    
            
    }	
		
	}
    
	
